import numpy as np
import tensorflow as tf
#load data
mesh = np.load('E:\GIT\dev1\mesh_feastnet\stiffened_plate_info_idx_53.npz')
colors = mesh.f.colors
labels = mesh.f.labels
names = mesh.f.names
triangles = mesh.f.triangles
vertices = mesh.f.vertices
print(vertices)
num_vertices = len(vertices)
num_triangles = len(triangles)
# name2 = tf.data.Dataset.from_tensor_slices(names)
# print(name2)
# #
# # vertices = tf.data.Dataset.from_tensor_slices(vertices.flatten())
# # name = tf.io.serialize_tensor(
# #     names, name=None
# # )
# #
# # triangles = tf.data.Dataset.from_tensor_slices(triangles.flatten())
#
# # vertices=tf.reshape(
# #     vertices, [1,-1], name=None)
# #
# # vertices = tf.io.serialize_tensor(
# #     vertices.flatten(), name=None
# # )
#
#
# # tf.io.serialize_sparse(vertices)
#
# # vertices=tf.reshape(
# #     vertices, [1,-1], name=None
# # )
# # print(vertices.take(10))
#
# # #
def _bytes_feature(value):
  """Returns a bytes_list from a string / byte."""
  if isinstance(value, type(tf.constant(0))):
    value = value.numpy() # BytesList won't unpack a string from an EagerTensor.
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

# print(_bytes_feature(name))
#
def _float_feature(value):
  """Returns a float_list from a float / double."""
  return tf.train.Feature(float_list=tf.train.FloatList(value=[i for i in value]))
# _float_feature(vertices)
# print(_float_feature(vertices))
# #
def _int64_feature(value):
  """Returns an int64_list from a bool / enum / int / uint."""
  return tf.train.Feature(int64_list=tf.train.Int64List(value=[i for i in value]))
# _int64_feature(triangles)


# get the data into a dataset
vertices = tf.data.Dataset.from_tensor_slices(( vertices.flatten()))
triangles = tf.data.Dataset.from_tensor_slices(( triangles.flatten()))
labels = tf.data.Dataset.from_tensor_slices(( labels.flatten()))
num_vertices = tf.data.Dataset.from_tensor_slices([num_vertices])
num_triangles = tf.data.Dataset.from_tensor_slices([num_triangles])

print(_int64_feature(num_vertices))
print(_int64_feature(num_triangles))
print(_int64_feature(labels))

feature = {
  'num_vertices': _int64_feature(num_vertices),
  'triangles ': _int64_feature(triangles),
  'labels':_int64_feature(labels),
  'num_triangles': _int64_feature(num_triangles),
  'vertices': _float_feature(vertices)
}
out_file = r"E:\GIT\dev1\mesh_feastnet\huan_test.tfrecords"
example_proto = tf.train.Example(
  features=tf.train.Features(feature=feature))
writer = tf.io.TFRecordWriter(out_file)
writer.write(example_proto.SerializeToString())

#serial all of them
# def serialize_example(num_vertices,triangles ,labels, num_triangles, vertices ):
#   """
#   Creates a tf.train.Example message ready to be written to a file.
#   """
#   # Create a dictionary mapping the feature name to the tf.train.Example-compatible
#   # data type.
#   feature = {
#     'num_vertices': _int64_feature(num_vertices),
#     'triangles ': _int64_feature(triangles),
#     'labels':_int64_feature(labels),
#     'num_triangles': _int64_feature(num_triangles),
#     'vertices': _float_feature(vertices)
#   }
#
#   # Create a Features message using tf.train.Example.
#
#   example_proto = tf.train.Example(features=tf.train.Features(feature=feature))
#   return example_proto.SerializeToString()
#
# def tf_serialize_example(f0,f1,f2,f3,f4):
#   tf_string = tf.py_function(
#     serialize_example,
#     (f0, f1, f2, f3, f4),  # Pass these args to the above function.
#     tf.string)      # The return type is `tf.string`.
#   return tf.reshape(tf_string, ()) # The result is a scalar.
#
# tf_serialize_example(num_vertices,triangles ,labels, num_triangles, vertices)
