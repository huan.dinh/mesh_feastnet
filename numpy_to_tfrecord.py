import numpy as np
import tensorflow as tf

#load data
mesh = np.load('E:\GIT\dev1\mesh_feastnet\stiffened_plate_info_idx_53.npz')
colors = mesh.f.colors
labels = mesh.f.labels
names = mesh.f.names
triangles = mesh.f.triangles
vertices = mesh.f.vertices
 # define data type
def _bytes_feature(value):
  """Returns a bytes_list from a string / byte."""
  if isinstance(value, type(tf.constant(0))):
    value = value.numpy() # BytesList won't unpack a string from an EagerTensor.
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=value.reshape(-1)))


def _float_feature(value):
  """Returns a float_list from a float / double."""
  return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))

def _int64_feature(value):
  """Returns an int64_list from a bool / enum / int / uint."""
  return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


# get the data into a dataset
stiffened_plate_dataset = tf.data.Dataset.from_tensor_slices(( vertices, labels, colors))
print(stiffened_plate_dataset)

# Creat a  tf.train.Example
def serialize_example(names ,vertices, labels, colors):
  """
  Creates a tf.train.Example message ready to be written to a file.
  """
  # Create a dictionary mapping the feature name to the tf.train.Example-compatible
  # data type.
  feature = {
      'names':   _bytes_feature(names),
      'vertices': _float_feature(vertices),
      'labels': _int64_feature(labels),
      'colors': _float_feature(colors),
  }

  # Create a Features message using tf.train.Example.

  example_proto = tf.train.Example(features=tf.train.Features(feature=feature))
  return example_proto.SerializeToString()

# serialized_example = serialize_example( vertices, labels, colors)
# print('example:', serialized_example)

#  specify the shape and type information
def tf_serialize_example(f0,f1,f2,f3):
  tf_string = tf.py_function(
    serialize_example,
    (f0, f1, f2, f3),  # Pass these args to the above function.
    tf.string)      # The return type is `tf.string`.
  return tf.reshape(tf_string, ()) # The result is a scalar.
# map
stiffened_plate_dataset = stiffened_plate_dataset.map(tf_serialize_example)
print(stiffened_plate_dataset)

for f0,f1,f2,f3 in stiffened_plate_dataset.take(1):
  print(f0)
  print(f1)
  print(f2)
  print(f3)

print("serial:",tf_serialize_example(f0, f1, f2, f3))

# create Genorator
def generator():
  for features in stiffened_plate_dataset:
    yield serialize_example(*features)
#
serialized_stiffened_plate_dataset = tf.data.Dataset.from_generator(
    generator, output_types=tf.string, output_shapes=())

print(serialized_stiffened_plate_dataset)

# write a file

filename = 'test_stif.tfrecord'
writer = tf.data.experimental.TFRecordWriter(filename)
writer.write(serialized_stiffened_plate_dataset)






