import tensorflow as tf
import numpy as np

filenames = r'E:\GIT\dev1\mesh_feastnet\huan_test.tfrecords'
raw_dataset = tf.data.TFRecordDataset(filenames)
print('raw data:',raw_dataset)

for raw_record in raw_dataset.take(1):
  example = tf.train.Example()
  example.ParseFromString(raw_record.numpy())
  print(example)

result = {}
# example.features.feature is the dictionary
for key, feature in example.features.feature.items():
  # The values are the Feature objects which contain a `kind` which contains:
  # one of three fields: bytes_list, float_list, int64_list

  kind = feature.WhichOneof('kind')
  print(key)
  result[key] = np.array(getattr(feature, kind).value)

print(result)

# result



# for raw_record in raw_dataset.take(10):
#   print(raw_record)

#
# # Create a description of the features.
# feature_description = {
#     'num_vertices': tf.io.FixedLenFeature([], tf.int64, default_value=0),
#     'triangles': tf.io.FixedLenFeature([], tf.int64, default_value=0),
#     'label':tf.io.FixedLenFeature([], tf.int64, default_value=0),
#     'num_triangles': tf.io.FixedLenFeature([], tf.int64, default_value=0),
#     'vertices': tf.io.FixedLenFeature([], tf.float64, default_value=0),
# }
#
#
#
#
# def _parse_function(example_proto):
#   # Parse the input `tf.train.Example` proto using the dictionary above.
#   return tf.io.parse_single_example(example_proto, feature_description)
#
# parsed_dataset = raw_dataset.map(_parse_function)
# print(parsed_dataset)
# for parsed_record in parsed_dataset.take(10):
#   print(repr(parsed_record))



